let today = new Date();
let dayInt = today.getDate();
let month = today.getMonth();
let year = today.getFullYear();

let calendarBody = document.getElementById("days");

let months = [
	"Jaanuar",
	"Veebruar",
	"Märts",
	"April",
	"Mai",
	"Juuni",
	"Juuli",
	"August",
	"September",
	"Oktoober",
	"November",
	"Detsember"
];

let nextbtn = document.getElementById("next");
let prevBtn = document.getElementById("prev");

nextbtn.onclick = function() {
	next();
};
prevBtn.onclick = function() {
	previous();
};

showCalendar(month, year);

function showCalendar(month, year) {
	let firstDay = new Date(year, month).getDay();
	calendarBody.innerHTML = "";
	let totalDays = daysInMonth(month, year);
	blankDates(firstDay);
	for (let day = 1; day <= totalDays; day++) {
		let cell = document.createElement("li");
		let cellText = document.createTextNode(day);
		if (
			dayInt === day &&
			month === today.getMonth() &&
			year === today.getFullYear()
		) {
			cell.classList.add("active");
		}

		cell.setAttribute("data-day", day);
		cell.setAttribute("data-month", month);
		cell.setAttribute("data-year", year);
		cell.setAttribute("onclick", "openModal()");

		cell.classList.add("singleDay");
		cell.appendChild(cellText);
		calendarBody.appendChild(cell);
	}

	document.getElementById("month").innerHTML = months[month];

	document.getElementById("year").innerHTML = year;
}

function daysInMonth(month, year) {
	return new Date(year, month + 1, 0).getDate();
}

function blankDates(count) {
	let blank = count === 0 ? 6 : count - 1;
	for (let x = 0; x < blank; x++) {
		let cell = document.createElement("li");
		let cellText = document.createTextNode("");
		cell.appendChild(cellText);
		calendarBody.appendChild(cell);
	}
}

function next() {
	year = month === 11 ? year + 1 : year;
	month = (month + 1) % 12;
	showCalendar(month, year);
}

function previous() {
	year = month === 0 ? year - 1 : year;
	month = month === 0 ? 11 : month - 1;
	showCalendar(month, year);
}

function openModal() {
	$("#toDoModal").modal("show");
}
