let currentActiveArticleId = 0;
window.addEventListener('DOMContentLoaded', () => {
    doLoadArticles(),
    displaySessionAdmin();
});

/*
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/


async function doLoadArticles() {
    let articles = await fetchArticles();
    displayArticles(articles);
}

async function doSaveArticle() {
    let newArticle = {
        id: currentActiveArticleId,
        heading: document.querySelector('#formaddheading').value,
        intro: document.querySelector('#formaddintro').value,
        body: document.querySelector('#formaddbody').value,
        type: document.querySelector('#formaddtype').value,
        focusId: document.querySelector('#formaddfocusId').value,
        active: document.querySelector('#formaddcheckbox').checked

    };
    let saveArticleId = await saveArticle(newArticle);
    currentActiveArticleId = saveArticleId;
    await doUpLoadFile();
    await doLoadArticles();
    doSortTable();
    // doResetTableFilters();
    resetForm();
    $('#addPopUp').modal('hide');
}

async function doUpLoadFile() {
    if (document.querySelector('#addfile').files.length > 0) {
        let newFile = {
            file: document.querySelector('#addfile').files[0],
            articleId: currentActiveArticleId
        };
        await uploadFile(newFile);
    }
}

function resetForm() {
    currentActiveArticleId = 0;
    document.querySelector('#formaddtype').value = ``;
    document.querySelector('#formaddheading').value = ``;
    document.querySelector('#formaddintro').value = ``;
    document.querySelector('#formaddbody').value = ``;
    document.querySelector('#formaddcheckbox').checked = false;
    document.querySelector('#formaddfocusId').value = ``;
    document.querySelector('#addfile').value = ``;
    document.querySelector('#placeHolderImg').innerHTML = ``;

}

async function editArticle(id = 0) {
    $('#addPopUp').modal('show');
    if (id > 0) {
        let article = await fetchArticle(id);
        document.querySelector('#formaddheading').value = article.heading;
        document.querySelector('#formaddintro').value = article.intro;
        document.querySelector('#formaddbody').value = article.body;
        document.querySelector('#formaddtype').value = article.type;
        document.querySelector('#formaddfocusId').value = article.focusId;
        document.querySelector('#formaddcheckbox').checked = article.active;
        if (article.files.length > 0) {
            let dynamicModalImg = ``;
            for (let i = 0; i < article.files.length; i++) {
                dynamicModalImg = dynamicModalImg + /*html*/
                    `
                    <div class="modalImg col-12 col-sm-6 col-md-4 col-lg-3">
                        <img src="${article.files[i].targetLocation}"/>
                        <button id="deleteImg" type="button" class="close" onclick="deleteFile('${article.files[i].generatedFileName}')"><span>&times;</span></button>  
                    </div>
                    `;
                document.querySelector('#placeHolderImg').innerHTML = dynamicModalImg;
            }
        } else {
            document.querySelector('#placeHolderImg').innerHTML = ``;
        }
        currentActiveArticleId = article.id;
    } else {
        resetForm();
    }
}

async function doDeleteArticle() {
    deleteArticle(currentActiveArticleId);
    $('#addPopUp').modal('hide');
}

function doLogout() {
    clearAuthentication();
    displaySessionAdmin();
}

/*
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function displayArticles(sortedArticles) {
    let sorted = sortedArticles;
    
    let articlesHTML = ``;
    for (let i = 0; i < sorted.length; i++) {
        articlesHTML = articlesHTML + /*html*/
            `
        <tr onclick="editArticle(${sorted[i].id})">
        <td class="ellipsis">${sorted[i].id}</td>
        <td>${
            sorted[i].type === 0 ? `Artikkel` :
                `${sorted[i].type === 1 ? `Blogi` :
                    `${sorted[i].type === 2 ? `Treeningud` :
                        `Pole väärtust`}
            `}
            `}
            </td>
            <td class="ellipsis1">${sorted[i].heading}</td>
            <td class="ellipsis1">${sorted[i].intro}</td>
            <td class="ellipsis1">${sorted[i].body}</td>
            <td class="ellipsis">${sorted[i].focusId}</td>
            <td class="ellipsis">${
                sorted[i].active === false ? `Ei` :
                `${sorted[i].active === true ? `Jah` :
                    `Pole Väärtust`}
                `}</td>
                </tr>
                `;
    }
    document.querySelector('#articleListRows').innerHTML = articlesHTML;
}


