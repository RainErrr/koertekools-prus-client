/*
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

async function doLogin() {
	const credentials = {
		username: document.querySelector("#inputUserName").value,
		password: document.querySelector("#inputPassword").value,
	};
	const session = await login(credentials);
	if (session) {
		storeAuthentication(session);

		if (getRoles() == "ROLE_ADMIN") {
			document.location = "admin.html";
		} else if (getRoles() == "ROLE_USER") {
			document.location = "user.html";
		} else if (getRoles() == "") {
			await clearAuthentication();
		}
	} else {
		console.log("Authentication failed");
	}
}

async function doRegister() {
	const credentials = {
		username: document.querySelector("#inputUserNameReg").value,
		password: document.querySelector("#inputPasswordReg").value,
	};
	if (
		document.querySelector("#inputPasswordReg").value ===
		document.querySelector("#repeatPassword").value
	) {
		await register(credentials);
		doResetRegisterModal();
		$("#registerSuccessModal").modal("show");
	} else {
		doResetRegisterModal();
		$("#registerFailedModal").modal("show");
	}
}
function doResetRegisterModal() {
	document.querySelector("#inputUserNameReg").value = "";
	document.querySelector("#inputPasswordReg").value = "";
	document.querySelector("#repeatPassword").value = "";
}

/*
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function displaySessionIndex() {
	const sessionIndexHome = document.querySelector("#navBarIndexHome");
	const sessionIndex = document.querySelector("#navBarIndex");
	if (getRoles() !== "") {
		if (getRoles() == "ROLE_ADMIN") {
			sessionIndexHome.innerHTML =
				/*html*/
				`
                <a class="nav-link" href="admin.html">Kodu</a>
            `;
		} else {
			sessionIndexHome.innerHTML =
				/*html*/
				`
                    <a class="nav-link" href="user.html">Kodu</a>
                `;
		}
		sessionIndex.innerHTML =
			/*html*/
			`
        <a class="nav-link" href="javascript:doLogout()"><i class="fas fa-user mr-2"></i><strong>${getUsername()}</strong> | Välju</a>
        `;
	} else {
		sessionIndex.innerHTML =
			/*html*/
			`
            <a class="nav-link" data-toggle="modal" data-target="#modalLRForm" href="modalLRForm">Logi sisse</a>
        `;
	}
}

function displaySessionAdmin() {
	const sessionAdmin = document.querySelector("#navBarAdmin");
	if (getRoles() == "ROLE_ADMIN") {
		sessionAdmin.innerHTML =
			/*html*/
			`
            <a class="nav-link" href="javascript:doLogout()"><i class="fas fa-user mr-2"></i><strong>${getUsername()}<strong> | Välju</a>
            `;
	} else {
		document.location = "index.html";
	}
}

function dispalySessionUser() {
	const sessionUserHome = document.querySelector("#navBarUserHome");
	const sessionUser = document.querySelector("#navBarUser");
	if (getRoles() !== "") {
		if (getRoles() == "ROLE_ADMIN") {
			sessionUserHome.innerHTML =
				/*html*/
				`
                    <a class="nav-link" href="admin.html">Kodu</a>
                `;
		} else {
			sessionUserHome.innerHTML =
				/*html*/
				`
                    <a class="nav-link" href="index.html">Kodu</a>
                `;
		}
		sessionUser.innerHTML =
			/*html*/
			`
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" 
                    href="#" 
                    id="navbarDropdown" 
                    role="button" 
                    data-toggle="dropdown" 
                    aria-haspopup="true" 
                    aria-expanded="false">
                    <i class="fas fa-user mr-2"></i><strong>${getUsername()}</strong>
                </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Treening päevik</a>
                        <a class="dropdown-item" href="#">Kasutaja sätted</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:doLogout()">Välju</a>
                    </div>
            </li>      
            `;
	} else {
		document.location = "index.html";
	}
}

function dispalySessionBlog() {
	const sessionBlogHome = document.querySelector("#navBarBlogHome");
	const sessionBlog = document.querySelector("#navBarBlog");
	if (getRoles() !== "") {
		if (getRoles() == "ROLE_ADMIN") {
			sessionBlogHome.innerHTML =
				/*html*/
				`
                    <a class="nav-link" href="admin.html">Kodu</a>
                `;
		} else {
			sessionBlogHome.innerHTML =
				/*html*/
				`
                    <a class="nav-link" href="user.html">Kodu</a>
                `;
		}
		sessionBlog.innerHTML =
			/*html*/
			`
                <a class="nav-link" href="javascript:doLogout()"><i class="fas fa-user mr-2"></i><strong>${getUsername()}</strong> | Välju</a>
            `;
	} else {
		sessionBlog.innerHTML =
			/*html*/
			`
            <a class="nav-link" data-toggle="modal" data-target="#modalLRForm" href="modalLRForm">Logi sisse</a>
        `;
	}
}

function dispalySessionDynamic() {
	const sessionDynamicHome = document.querySelector("#navBarDynamicHome");
	const sessionDynamic = document.querySelector("#navBarDynamic");
	if (getRoles() !== "") {
		if (getRoles() == "ROLE_ADMIN") {
			sessionDynamicHome.innerHTML =
				/*html*/
				`
                    <a class="nav-link" href="admin.html">Kodu</a>
                `;
		} else {
			sessionDynamicHome.innerHTML =
				/*html*/
				`
                    <a class="nav-link" href="user.html">Kodu</a>
                `;
		}
		sessionDynamic.innerHTML =
			/*html*/
			`
                <a class="nav-link" href="javascript:doLogout()"><i class="fas fa-user mr-2"></i><strong>${getUsername()}</strong> | Välju</a>
            `;
	} else {
		sessionDynamic.innerHTML =
			/*html*/
			`
            <a class="nav-link" data-toggle="modal" data-target="#modalLRForm" href="modalLRForm">Logi sisse</a>
        `;
	}
}
