window.addEventListener('DOMContentLoaded', () => {
    dispalyDynamicPage();
    dispalySessionDynamic()
});

/*
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

function doLogout() {
    clearAuthentication();
    dispalySessionDynamic()
}

/*
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

const urlParams = new URLSearchParams(window.location.search);
const articleId = urlParams.get('article_id');

async function dispalyDynamicPage() {
    let article = await fetchArticle(articleId);
    let dynamicPageArticle = ``;
    dynamicPageArticle = dynamicPageArticle + /*html*/
        `
        <div class="topbanner">
            <img src= "${
                article.files.length === 0 ? `` : 
                    `${article.files.length > 0 ? `${article.files[0].targetLocation}` : 
                    ``}
                `}"
            alt="${article.heading}">
            </div>
        <div class="container">
            <div class="text">
                <h2>${article.heading}</h2>
                <p>${article.body}</p>
            </div>
        </div>
        `; 
    document.querySelector('#displayOnDynamicPage').innerHTML = dynamicPageArticle;
}

