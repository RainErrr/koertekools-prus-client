const AUTH_TOKEN = "AUTH_TOKEN";
const AUTH_USERNAME = "AUTH_USERNAME";
const AUTH_ROLES = "AUTH_ROLES";

async function clearAuthentication() {
    localStorage.removeItem(AUTH_TOKEN);
    localStorage.removeItem(AUTH_USERNAME);
    localStorage.removeItem(AUTH_ROLES);
}

function storeAuthentication(session) {
    localStorage.setItem(AUTH_TOKEN, session.token);
    localStorage.setItem(AUTH_USERNAME, session.username);
    localStorage.setItem(AUTH_ROLES, JSON.stringify(session.roles));
}

function getUsername() {
    return localStorage.getItem(AUTH_USERNAME);
}

function getToken() {
    return localStorage.getItem(AUTH_TOKEN);
}

function getRoles() {
    let roles = localStorage.getItem(AUTH_ROLES);
    return roles ? JSON.parse(roles) : '';
}
