window.addEventListener('DOMContentLoaded', () => { 
    dispalySessionBlog(),
    displayArticlesBlog();
});

/*
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

function doLogout() {
    clearAuthentication();
    dispalySessionBlog();
}

/*
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

async function displayArticlesBlog() {
    let articles = await fetchActiveArticles();
    let articlesHTML = ``;
    for (let i = 0; i < articles.length; i++) {
        if (articles[i].type === 1) {
            articlesHTML = articlesHTML + /*html*/
                `
                <div class="box col-12 col-sm-12 col-md-6 col-xl-4">
                    <div class="bginnerpicture">
                        <a href="dynamicpage.html?article_id=${articles[i].id}">
                        <img src="${
                            articles[i].files.length === 0 ? `` : 
                                `${articles[i].files.length > 0 ? `${articles[i].files[0].targetLocation}   ` : ``
                                }
                            `}"
                        alt="${articles[i].heading}">
                    </div>
                        <h2 class="heading">${articles[i].heading}</h2></a>
                </div>
                `;    
        }
    }   
    document.querySelector('#articleForBlog').innerHTML = articlesHTML; 
}

function openArticle(articleId) {
    document.location = "dynamicpage.html?article_id=" + article_id;
}

// <a href="dynamicpage.html">
//<h2 class="heading">${articles[i].heading}</h2></a>