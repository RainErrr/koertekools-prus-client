let currentActiveArticleId = 0;
window.addEventListener('DOMContentLoaded', () => { displaySessionIndex(), displayCarouselItem(), displayTrainings(); });

function doLogout() {
    clearAuthentication();
    displaySessionIndex();
}

/*
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

async function displayTrainings() {
    let articles = await fetchActiveArticles();
    let articlesHTML = ``;
    let counter = 0;
    let counter2 = 0;

    for (let i = 0; i < articles.length; i++) {
        if (articles[i].type === 2) {
            counter2 = counter2 + 2;
            counter = counter + 1;
            if (counter % 2 != 0) {
                articlesHTML = articlesHTML + /*html*/
                    `
                <div class="row padding" id="trainingjumbo">
                <div class="col-12 col-lg-6 order-${counter2 - 1} order-sm-${counter2 - 1} order-md-${counter2 - 1} order-lg-${counter2 - 1}">
                    <div class="content">
                        <h2>${articles[i].heading}</h2>
                        <p>${articles[i].body}</p>
                    </div>
                </div>
                <div class="col-12 col-lg-6 order-${counter2} order-sm-${counter2} order-md-${counter2} order-lg-${counter2}">
                    <img src="${
                    articles[i].files.length === 0 ? `` :
                        `${articles[i].files.length > 0 ? `${articles[i].files[0].targetLocation}` :
                            ``}
                        `}" class="img-fluid">
                </div>
                </div>
                `;
            } else {
                articlesHTML = articlesHTML + /*html*/
                    `
                <div class="row padding" id="trainingjumbo">
                    <div class="col-12 col-lg-6 order-${counter2} order-sm-${counter2} order-md-${counter2} order-lg-${counter2 - 1}">
                        <img src="${
                    articles[i].files.length === 0 ? `` :
                        `${articles[i].files.length > 0 ? `${articles[i].files[0].targetLocation}` :
                            ``}
                            `}" class="img-fluid">
                    </div>
                    <div class="col-12 col-lg-6 order-${counter2 - 1} order-sm-${counter2 - 1} order-md-${counter2 - 1} order-lg-${counter2}">
                        <div class="content">
                            <h2>${articles[i].heading}</h2>
                            <p>${articles[i].body}</p>
                        </div>
                    </div>
                </div>
                `;
            }
        }
    }
    document.querySelector('#trainingForMainPage').innerHTML = articlesHTML;
}

async function displayCarouselItem() {
    let carousel = await fetchActiveArticles();
    let carouselHTML = ``;
    for (let i = 0; i < carousel.length; i++) {
        if (carousel[i].focusId === 1) {
            carouselHTML = carouselHTML + /*html*/
                `
                <div class="carousel-item active">
                    <img src="${
                        carousel[i].files.length === 0 ? `` :
                            `${carousel[i].files.length > 0 ? `${carousel[i].files[0].targetLocation}` : ``
                            }
                        `}"
                    alt="${carousel[i].heading}">
                    <div class="carousel-caption">
                        <h1 class="dispaly-2" id="article1">${carousel[i].heading}</h1>
                        <a href="dynamicpage.html?article_id=${carousel[i].id}" class="btn btn-outline-light btn-lg full-width" role="button">Mine</a>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#slides" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			</a>
			<a class="carousel-control-next" href="#slides" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
			</a>
                `;
        } else if (carousel[i].focusId > 1) {
            carouselHTML = carouselHTML + /*html*/
            `
            <div class="carousel-item">
                <img src="${
                    carousel[i].files.length === 0 ? `` :
                        `${carousel[i].files.length > 0 ? `${carousel[i].files[0].targetLocation}` : ``
                        }
                    `}"
                alt="${carousel[i].heading}">
                <div class="carousel-caption">
                    <h1 class="dispaly-2" id="article1">${carousel[i].heading}</h1>
                    <a href="dynamicpage.html?article_id=${carousel[i].id}" class="btn btn-outline-light btn-lg full-width" role="button">Mine</a>
                </div>
            </div>
            <a class="carousel-control-prev" href="#slides" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			</a>
			<a class="carousel-control-next" href="#slides" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
			</a>
            `;
        }
    }
    document.querySelector('#carouselDynamicItem').innerHTML = carouselHTML;
}

function openArticle(articleId) {
    document.location = "dynamicpage.html?article_id=" + article_id;
}