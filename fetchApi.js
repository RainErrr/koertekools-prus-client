async function login(credentials) {
    let response = await fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    );
    if (response.status >= 400) {
        return null;
    } else {
        return await response.json();
    } 
}

async function register(credentials) {
    let response = await fetch(
        `${API_URL}/users/register`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    );
    return await response.json();
}

async function fetchArticles() {
    try {
        let response = await fetch('http://localhost:8080/articles'
        , {
        headers: {'Authorization': `Bearer ${getToken()}`}
        }
        );
        let articles = await response.json();
        return articles;
    } catch (e) {
        console.log('Kuskil on jama');

    }
}

async function fetchActiveArticles() {
    try {
        let response = await fetch('http://localhost:8080/articles/active'
        );
        let articles = await response.json();
        return articles;
    } catch (e) {
        console.log('Kuskil on jama');
    }
}

async function fetchArticle(id) {
    try {
        let response = await fetch('http://localhost:8080/articles/' + id);
        let article = await response.json();
        return article;
    } catch (e) {
        console.log('Viga', e);
    }
}

async function saveArticle(article) {
    let requestUrl = API_URL + '/articles';
    let conf = {
        method: 'POST',
        headers: {'Authorization': `Bearer ${getToken()}`,'Content-Type': 'application/json',},
        body: JSON.stringify(article)
    };
    let result = await fetch(requestUrl, conf);
    let body = await result.json();
    return body;
}

async function deleteArticle(id) {
    let result = await fetch(API_URL + '/articles/' + id, {
        method: 'DELETE',
        headers: {'Authorization': `Bearer ${getToken()}`}    
    });
    await doLoadArticles();
}

async function deleteFile(fileName) {
    let result = await fetch(API_URL + '/files/' + fileName, {
        method: 'DELETE',
        headers: {'Authorization': `Bearer ${getToken()}`} 
    });
    await editArticle(currentActiveArticleId);
}

async function uploadFile(newFile) {
    let formData = new FormData();
    formData.append('file', newFile.file)
    formData.append('articleId', newFile.articleId)
    let requestUrl = API_URL + '/files/upload';
    let conf = {
        method: 'POST',
        body: formData,
        headers: {'Authorization': `Bearer ${getToken()}`}
    };
    let result = await fetch(requestUrl, conf);
}