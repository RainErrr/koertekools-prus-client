async function gettingArticles() {
    return articles;
}


async function doSortTable() {
    let articles = await fetchArticles();
    let sortedArticles = articles;

    // if(document.querySelector('#sortOptionsByType').value === "0") {
    //     sortedArticles = sortedArticles.filter(articles => articles.type === 0);
    // } 
    // if (document.querySelector('#sortOptionsByType').value === "1") {
    //     sortedArticles = sortedArticles.filter(articles => articles.type === 1);
    // }
    // if (document.querySelector('#sortOptionsByType').value === "2") {
    //     sortedArticles = sortedArticles.filter(articles => articles.type === 2);
    // }

    if (document.querySelector('#sortOptionsByType').value !== "" && document.querySelector('#sortOptionsByType').value !== '3' ) {
        let conditionValue = parseInt(document.querySelector('#sortOptionsByType').value);
        sortedArticles = sortedArticles.filter(articles => articles.type === conditionValue);
    }

    // let conditionValue = parseInt(document.querySelector('#sortOptionsByType').value);
    // sortedArticles = sortedArticles.filter(articles => conditionValue === 0 || articles.type === conditionValue);

    // if (document.querySelector('#sortOptionsByType').value === "3") {
    // }
    if (document.querySelector('#sortOptionsByFocus').value === "0") {
    }
    if (document.querySelector('#sortOptionsByFocus').value === "1") {
        sortedArticles = sortedArticles.filter(articles => articles.focusId > 0);
    }
    if (document.querySelector('#sortOptionsByFocus').value === "2") {
        sortedArticles = sortedArticles.filter(articles => articles.focusId === 0);
    }
    if (document.querySelector('#sortOptionsByActive').value === "0") {
    }
    if (document.querySelector('#sortOptionsByActive').value === "1") {
        sortedArticles = sortedArticles.filter(articles => articles.active === true);
    }
    if (document.querySelector('#sortOptionsByActive').value === "2") {
        sortedArticles = sortedArticles.filter(articles => articles.active === false);
    }
    displayArticles(sortedArticles);
    return sortedArticles;
}

function doResetTableFilters() {
    document.querySelector('#sortOptionsByType').value = '3';
    document.querySelector('#sortOptionsByFocus').value = '0';
    document.querySelector('#sortOptionsByActive').value = '0';
    doSortTable();
}


// const tableData = () => {
//     const searchData = doSortTable();
//     const tableEl = document.getElementById('mainAdminTable');
//     Array.from(tableEl.children[1].children).forEach(_bodyRowEl => {
//         searchData.push(Array.from(_bodyRowEl.children).map(_cellEl => {
//             return _cellEl.innerHTML;
//         }));
//     });
//     return searchData; 
// }


// const createSearchInputElement = () => {
//     const el = document.createElement('input');
//     el.classList.add('search-input');
//     el.id = 'search-input';
//     return el;
// }

// const search = (searchTerm) => {
//     if(!searchTerm) return;

//    let sortedArticles = doSortTable();
    
//     let searchedArticles = sortedArticles.filter(article => 
//         article.heading.toLowerCase().includes(searchTerm.toLowerCase())
//         || article.intro.toLowerCase().includes(searchTerm.toLowerCase()) 
//         || article.body.toLowerCase().includes(searchTerm.toLowerCase())
//     );
//     return displayArticles(searchedArticles)
// };


// const init = () => {
//     document.getElementById('rootTableSearch').appendChild(createSearchInputElement());
//     // const initialTableData = tableData();
//     const searchInput = document.getElementById('search-input');
//     searchInput.addEventListener('keyup', (e) => {
//         search(searchInput.value);
//         // console.log(tableData());
//     });
//     fetchArticles().then((result) => articles = result);
// }

// init();


/////////
// PROOV
/////////


// const search = (arr, searchTerm) => {
//     if(!searchTerm) return arr;

//     return arr.filter(_row => {
//         return _row.find(_item => _item.toLowerCase().includes(searchTerm.toLowerCase()))
//     });
// }



// const refreshTable = (data) => {
//     const tableBody = document.getElementById('mainAdminTable').children[1];
//     tableBody.innerHTML = '';

//     data.forEach(_row => {
//         const curRow = document.createElement('tr');
//         _row.forEach(_dataItem => {
//             const curCell = document.createElement('td');
//             curCell.innerHTML = _dataItem;
//             curRow.appendChild(curCell);
//         });
//         tableBody.appendChild(curRow);
//     });
// }

// const init = () => {
//     document.getElementById('rootTableSearch').appendChild(createSearchInputElement());

//     const initialTableData = tableData();

//     const searchInput = document.getElementById('search-input');
//     searchInput.addEventListener('keyup', (e) => {
//         search(tableData(), e.target.value);
        
//         console.log(tableData());
        
//     });

// }

// init()